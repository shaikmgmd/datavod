const mongoose = require("mongoose");
const connectionParameters = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};
const connectDB = async () => {
  try {
    const conn = await mongoose.connect(process.env.DB, connectionParameters);
    console.log("Connected to database");
  } catch (err) {
    console.error(err);
  }
};

module.exports = connectDB;
