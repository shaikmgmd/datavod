const express = require("express");
const {
  getVideos,
  CreateVideo,
  getVideoById,
  UpdateVideo,
  DeleteVideo,
  getAllVideos,
  getInfosOfVideo,
  getUserOfVideo,
  getUserVideos,
} = require("../controllers/videoControllers");
const { protect } = require("../middlewares/authMiddleware");
const router = express.Router();

router.route("/").get(protect, getVideos);
router.route("/uservideos/:id").get(getUserVideos);
router.route("/create").post(protect, CreateVideo);
router.route("/allvideos").get(getAllVideos);
router.route("/:id").get(getVideoById);
router.route("/watch/:id").get(getInfosOfVideo);
router.route("/:id").put(protect, UpdateVideo);
router.route("/search/:id/:idUser").get(getUserOfVideo);
router.route("/:id").delete(protect, DeleteVideo);
module.exports = router;
