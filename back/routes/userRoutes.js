const express = require("express");
const {
  registerUser,
  authUser,
  getUsers,
  updateUserProfile,
  getUser,
} = require("../controllers/userControllers");
const { protect } = require("../middlewares/authMiddleware");
const router = express.Router();

router.route("/").post(registerUser);
router.route("/login").post(authUser);
router.route("/infos/:id").get(getUser);
router.route("/allusers").get(getUsers);
router.route("/profile").post(protect, updateUserProfile);
module.exports = router;
