const Video = require("../models/videoModel");
const User = require("../models/userModel");

const asyncHandler = require("express-async-handler");

const getVideos = asyncHandler(async (req, res) => {
  const videos = await Video.find({ user: req.user._id });
  res.json(videos);
});

const getUserVideos = asyncHandler(async (req, res) => {
  const videos = await Video.find({ user: req.params.id });
  res.json(videos);
});

const getAllVideos = asyncHandler(async (req, res) => {
  const videos = await Video.find();
  res.json(videos);
});

const getInfosOfVideo = asyncHandler(async (req, res) => {
  const video = await Video.find({ _id: req.params.id });
  res.json(video);
});

const getUserOfVideo = asyncHandler(async (req, res) => {
  const user = await User.find({ _id: req.params.idUser });
  if (user) {
    res.json(user);
  } else {
    res.status(404).json({ message: "L'utilisateur n'a pas été trouvée..." });
  }
});

const CreateVideo = asyncHandler(async (req, res) => {
  const { title, description, videoPath } = req.body;

  if (!title || !description || !videoPath) {
    res.status(400);
    throw new Error("Veuillez remplir tous les champs");
  } else {
    const video = new Video({
      user: req.user._id,
      title,
      description,
      videoPath,
      nbVue: 0,
    });

    const createdVideo = await video.save();

    res.status(201).json(createdVideo);
  }
});

const getVideoById = asyncHandler(async (req, res) => {
  const video = await Video.findById(req.params.id);

  if (video) {
    res.json(video);
  } else {
    res.status(404).json({ message: "La vidéo n'a pas été trouvée..." });
  }
});

const UpdateVideo = asyncHandler(async (req, res) => {
  const { title, description, videoPath } = req.body;
  const video = await Video.findById(req.params.id);

  if (video.user.toString() !== req.user._id.toString()) {
    res.status(401);
    throw new Error("Vous ne pouvez pas faire cela");
  }
  if (video) {
    video.title = title;
    video.description = description;
    video.videoPath = videoPath;

    const updatedVideo = await video.save();
    res.json(updatedVideo);
  } else {
    res.status(404);
    throw new Error("La vidéo n'a pas été trouvée...");
  }
});

const DeleteVideo = asyncHandler(async (req, res) => {
  const video = await Video.findById(req.params.id);

  if (video.user.toString() !== req.user._id.toString()) {
    res.status(401);
    throw new Error("Vous ne pouvez pas faire cela");
  }

  if (video) {
    await video.remove();
    res.json({ message: "Vidéo supprimé" });
  } else {
    res.status(404);
    throw new Error("La vidéo n'a pas été trouvée...");
  }
});

module.exports = {
  getVideos,
  CreateVideo,
  getVideoById,
  UpdateVideo,
  DeleteVideo,
  getAllVideos,
  getInfosOfVideo,
  getUserOfVideo,
  getUserVideos,
};
