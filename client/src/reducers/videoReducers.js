import {
  VIDEOS_CREATE_FAIL,
  VIDEOS_CREATE_REQUEST,
  VIDEOS_CREATE_SUCCESS,
  VIDEOS_DELETE_FAIL,
  VIDEOS_DELETE_REQUEST,
  VIDEOS_DELETE_SUCCESS,
  VIDEOS_LIST_FAIL,
  VIDEOS_LIST_REQUEST,
  VIDEOS_LIST_SUCCESS,
  VIDEOS_UPDATE_FAIL,
  VIDEOS_UPDATE_REQUEST,
  VIDEOS_UPDATE_SUCCESS,
} from "../constants/videosConstants";

export const videoListReducer = (state = { videos: [] }, action) => {
  switch (action.type) {
    case VIDEOS_LIST_REQUEST:
      return { loading: true };
    case VIDEOS_LIST_SUCCESS:
      return { loading: false, videos: action.payload };
    case VIDEOS_LIST_FAIL:
      return { loading: false, error: action.payload };

    default:
      return state;
  }
};

export const videoCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case VIDEOS_CREATE_REQUEST:
      return { loading: true };
    case VIDEOS_CREATE_SUCCESS:
      return { loading: false, success: true };
    case VIDEOS_CREATE_FAIL:
      return { loading: false, error: action.payload };

    default:
      return state;
  }
};

export const videoUpdateReducer = (state = {}, action) => {
  switch (action.type) {
    case VIDEOS_UPDATE_REQUEST:
      return { loading: true };
    case VIDEOS_UPDATE_SUCCESS:
      return { loading: false, success: true };
    case VIDEOS_UPDATE_FAIL:
      return { loading: false, error: action.payload, success: false };

    default:
      return state;
  }
};

export const videoDeleteReducer = (state = {}, action) => {
  switch (action.type) {
    case VIDEOS_DELETE_REQUEST:
      return { loading: true };
    case VIDEOS_DELETE_SUCCESS:
      return { loading: false, success: true };
    case VIDEOS_DELETE_FAIL:
      return { loading: false, error: action.payload, success: false };

    default:
      return state;
  }
};
