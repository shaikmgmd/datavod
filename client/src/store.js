import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import {
  videoCreateReducer,
  videoDeleteReducer,
  videoListReducer,
  videoUpdateReducer,
} from "./reducers/videoReducers";
import { composeWithDevTools } from "redux-devtools-extension";

import {
  userLoginReducer,
  userRegisterReducer,
  userUpdateReducer,
} from "./reducers/userReducers";

const reducer = combineReducers({
  videoList: videoListReducer,
  userLogin: userLoginReducer,
  userRegister: userRegisterReducer,
  videoCreate: videoCreateReducer,
  videoDelete: videoDeleteReducer,
  videoUpdate: videoUpdateReducer,
  userUpdate: userUpdateReducer,
});

const userInfoFromStorage = localStorage.getItem("userInfo")
  ? JSON.parse(localStorage.getItem("userInfo"))
  : null;

const initialState = {
  userLogin: { userInfo: userInfoFromStorage },
};

const middleware = [thunk];

const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
