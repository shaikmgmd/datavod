import axios from "axios";
import {
  VIDEOS_CREATE_FAIL,
  VIDEOS_CREATE_REQUEST,
  VIDEOS_CREATE_SUCCESS,
  VIDEOS_DELETE_FAIL,
  VIDEOS_DELETE_REQUEST,
  VIDEOS_DELETE_SUCCESS,
  VIDEOS_LIST_FAIL,
  VIDEOS_LIST_REQUEST,
  VIDEOS_LIST_SUCCESS,
  VIDEOS_UPDATE_FAIL,
  VIDEOS_UPDATE_REQUEST,
  VIDEOS_UPDATE_SUCCESS,
} from "../constants/videosConstants";

export const listVideos = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: VIDEOS_LIST_REQUEST,
    });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    const { data } = await axios.get(`/api/videos`, config);

    dispatch({
      type: VIDEOS_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: VIDEOS_LIST_FAIL,
      payload: message,
    });
  }
};

export const createVideoAction =
  (title, description, videoPath) => async (dispatch, getState) => {
    try {
      dispatch({
        type: VIDEOS_CREATE_REQUEST,
      });

      const {
        userLogin: { userInfo },
      } = getState();

      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userInfo.token}`,
        },
      };

      const { data } = await axios.post(
        `/api/videos/create`,
        { title, description, videoPath },
        config
      );

      dispatch({
        type: VIDEOS_CREATE_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: VIDEOS_CREATE_FAIL,
        payload: message,
      });
    }
  };

export const updateVideoAction =
  (id, title, description, videoPath) => async (dispatch, getState) => {
    try {
      dispatch({
        type: VIDEOS_UPDATE_REQUEST,
      });

      const {
        userLogin: { userInfo },
      } = getState();

      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userInfo.token}`,
        },
      };

      const { data } = await axios.put(
        `/api/videos/${id}`,
        { title, description, videoPath },
        config
      );

      dispatch({
        type: VIDEOS_UPDATE_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: VIDEOS_UPDATE_FAIL,
        payload: message,
      });
    }
  };

export const deleteVideoAction = (id) => async (dispatch, getState) => {
  try {
    dispatch({
      type: VIDEOS_DELETE_REQUEST,
    });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    const { data } = await axios.delete(`/api/videos/${id}`, config);

    dispatch({
      type: VIDEOS_DELETE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: VIDEOS_DELETE_FAIL,
      payload: message,
    });
  }
};
