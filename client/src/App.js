import { Route, Routes } from "react-router-dom";
import "./bootstrap.min.css";
import Navbar from "./components/Navbar/Header";
import MyVideos from "./screens/MyVideos/MyVideos";
import LandingPage from "./screens/LandingPage";
import LoginScreen from "./screens/LoginScreen/LoginScreen";
import RegisterScreen from "./screens/RegisterScreen/RegisterScreen";
import CreateVideoScreen from "./screens/CreateVideoScreen/CreateVideoScreen";
import SingleVideo from "./screens/SingleVideo";
import ProfileScreen from "./screens/ProfileScreen/ProfileScreen";
import VideoList from "./screens/VideoList/VideoList";
import WatchScreen from "./screens/WatchScreen/WatchScreen";
import UserVideos from "./screens/WatchScreen/UserVideos";
import UsersScreen from "./screens/UsersScreen/UsersScreen";

function App() {
  return (
    <>
      <Navbar />
      <main>
        <Routes>
          <Route path="/" exact element={<LandingPage />} />
          <Route path="/accueil" exact element={<VideoList />} />
          <Route path="/userslist" exact element={<UsersScreen />} />
          <Route path="/login" exact element={<LoginScreen />} />
          <Route path="/register" exact element={<RegisterScreen />} />
          <Route path="/myvideos" exact element={<MyVideos />} />
          <Route path="/createvideo" exact element={<CreateVideoScreen />} />
          <Route path="/video/:id" exact element={<SingleVideo />} />
          <Route path="/profile" exact element={<ProfileScreen />} />
          <Route path="/watch/:id/:idUser" exact element={<WatchScreen />} />
          <Route path="/uservideos/:idUser" exact element={<UserVideos />} />
        </Routes>
      </main>
    </>
  );
}

export default App;
