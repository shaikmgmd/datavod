import React, { useEffect } from "react";
import { Container, Nav, NavDropdown } from "react-bootstrap";
import Navbar from "react-bootstrap/Navbar";
import { Link, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlay } from "@fortawesome/free-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../actions/userActions";
import "./Header.css";

const Header = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const logoutHandler = () => {
    dispatch(logout());
    navigate("/");
  };

  useEffect(() => {}, [userInfo]);
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="/">
          <Link to="/">
            DataV
            <FontAwesomeIcon
              icon={faPlay}
              className="playIconNav"
              style={{ marginLeft: 2, marginRight: 1.5, fontSize: 17 }}
            />
            D
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          {userInfo ? (
            <Nav className="me-auto" style={{ marginLeft: 20 }}>
              <Nav.Link href="/accueil">
                <Link to="/accueil">Accueil</Link>
              </Nav.Link>
              <Nav.Link href="/userslist">
                <Link to="/userslist">Utilisateurs</Link>
              </Nav.Link>
              <Nav.Link href="/myvideos">
                <Link to="/myvideos">Vidéos</Link>
              </Nav.Link>

              <NavDropdown title={userInfo?.userName} id="basic-nav-dropdown">
                <NavDropdown.Item href="/profile">
                  <Link to="/profile">Mon profil</Link>
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item onClick={logoutHandler}>
                  Déconnexion
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          ) : (
            <Nav>
              <Nav.Link></Nav.Link>
            </Nav>
          )}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
