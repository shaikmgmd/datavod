import React, { useRef, useState } from "react";
import MainScreen from "../../components/MainScreen";
import { Col, Button, Card, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import Loading from "../../components/Loading";
import ErrorMessage from "../../components/ErrorMessage";
import { useNavigate } from "react-router-dom";
import { createVideoAction } from "../../actions/videoActions";

const CreateVideoScreen = () => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [videoPath, setVideoPath] = useState("");
  const [videoMessage, setVideoMessage] = useState(null);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const videoCreate = useSelector((state) => state.videoCreate);
  const { loading, error, video } = videoCreate;

  const resetHandler = () => {
    setTitle("");
    setDescription("");
    setVideoPath("");
  };

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(createVideoAction(title, description, videoPath));
    if (!title || !description || !videoPath) return;
    resetHandler();
    navigate("/myvideos");
  };

  const inputRef = useRef(null);

  const handleUpload = (e) => {
    e.preventDefault();
    inputRef.current?.click();
  };

  const handleDisplayFileDetails = () => {
    inputRef.current?.files && setVideoPath(inputRef.current.files[0].name);
  };

  const postDetails = (videoPath) => {
    setVideoPath("Patientiez...");
    if (videoPath === undefined || videoPath === "") {
      return setVideoMessage("Selectionnez une vidéo svp");
    }
    setVideoMessage(null);
    if (videoPath.type === "video/mkv" || videoPath.type === "video/mp4") {
      const data = new FormData();
      data.append("file", videoPath);
      data.append("upload_preset", "datavod");
      data.append("cloud_name", "du4tmosfx");
      fetch("https://api.cloudinary.com/v1_1/du4tmosfx/video/upload", {
        method: "post",
        body: data,
      })
        .then((res) => res.json())
        .then((data) => {
          setVideoPath(data.url.toString());
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      return setVideoMessage("Sélectionnez une vidéo svp");
    }
  };

  return (
    <MainScreen title="Ajouter une vidéo">
      <Card>
        <Card.Header>Créer une vidéo</Card.Header>
        <Card.Body>
          <Form onSubmit={submitHandler}>
            {error && <ErrorMessage variant="danger">{error}</ErrorMessage>}
            <Form.Group controlId="title">
              <Form.Label>Titre</Form.Label>
              <Form.Control
                type="title"
                value={title}
                placeholder="Entrez le titre"
                onChange={(e) => setTitle(e.target.value)}
              />
            </Form.Group>

            <Form.Group
              controlId="description"
              style={{ marginTop: "15px", marginBottom: "10px" }}
            >
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                value={description}
                placeholder="Entrez la description de la vidéo"
                rows={4}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>
            {videoMessage && (
              <ErrorMessage variant="danger">{videoMessage}</ErrorMessage>
            )}
            <Form.Group controlId="pic">
              <div className="my-3">
                <label className="mx-3">Choisir une vidéo : &lt; 100 Mo</label>
                <input
                  ref={inputRef}
                  onChange={(e) => postDetails(e.target.files[0])}
                  className="d-none"
                  type="file"
                />
                <button
                  onClick={handleUpload}
                  className={`btn btn-outline-${
                    videoPath ? "success" : "primary"
                  }`}
                >
                  {videoPath ? videoPath : "Parcourir"}
                </button>
              </div>
            </Form.Group>
            {loading && <Loading size={50} />}
            <Button type="submit" variant="primary">
              Ajouter
            </Button>
            <Button className="mx-2" onClick={resetHandler} variant="danger">
              Reset
            </Button>
          </Form>
        </Card.Body>

        <Card.Footer className="text-muted">
          Ajoutée le - {new Date().toLocaleDateString()}
        </Card.Footer>
      </Card>
    </MainScreen>
  );
};

export default CreateVideoScreen;
