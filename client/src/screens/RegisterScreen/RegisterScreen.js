import React, { useState, useEffect } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import Loading from "../../components/Loading";
import ErrorMessage from "../../components/ErrorMessage";
import MainScreen from "../../components/MainScreen";
// import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { register } from "../../actions/userActions";

const RegisterScreen = ({ history }) => {
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [message, setMessage] = useState(null);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const userRegister = useSelector((state) => state.userRegister);
  const { loading, error, userInfo } = userRegister;

  const submitHandler = async (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      setMessage("Les mots de passe sont différents");
    } else {
      dispatch(register(firstName, lastName, userName, email, password));
    }
  };

  useEffect(() => {
    if (userInfo) {
      navigate("/myvideos");
    }
  }, [history, userInfo]);

  return (
    <MainScreen title="S'inscrire">
      <div
        className="registerContainer"
        style={{ display: "flex", flexDirection: "column", margin: "20px" }}
      >
        {error && <ErrorMessage variant="danger">{error}</ErrorMessage>}
        {message && <ErrorMessage variant="danger">{message}</ErrorMessage>}
        {loading && <Loading />}
        <Form onSubmit={submitHandler}>
          <Form.Group controlId="firstName">
            <Form.Label>Prénom</Form.Label>
            <Form.Control
              type="name"
              value={firstName}
              placeholder="Entrez votre prénom"
              onChange={(e) => setFirstName(e.target.value)}
            />
          </Form.Group>
          <Form.Group
            controlId="lastName"
            style={{ marginTop: "15px", marginBottom: "10px" }}
          >
            <Form.Label>Nom</Form.Label>
            <Form.Control
              type="name"
              value={lastName}
              placeholder="Entrez votre nom"
              onChange={(e) => setLastName(e.target.value)}
            />
          </Form.Group>
          <Form.Group
            controlId="userName"
            style={{ marginTop: "15px", marginBottom: "10px" }}
          >
            <Form.Label>Nom d'utilisateur</Form.Label>
            <Form.Control
              type="name"
              value={userName}
              placeholder="Entrez votre nom d'utilisateur"
              onChange={(e) => setUserName(e.target.value)}
            />
          </Form.Group>

          <Form.Group
            controlId="formBasicEmail"
            style={{ marginTop: "15px", marginBottom: "10px" }}
          >
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              value={email}
              placeholder="Entrez votre email"
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>

          <Form.Group
            controlId="formBasicPassword"
            style={{ marginTop: "15px", marginBottom: "10px" }}
          >
            <Form.Label>Mot de passe</Form.Label>
            <Form.Control
              type="password"
              value={password}
              placeholder="Entrez votre mot de passe"
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="confirmPassword" style={{ marginTop: "15px" }}>
            <Form.Label>Confirmer mot de passe</Form.Label>
            <Form.Control
              type="password"
              value={confirmPassword}
              placeholder="Confirmez votre mot de passe"
              onChange={(e) => setConfirmPassword(e.target.value)}
              //required
            />
          </Form.Group>

          <Button variant="primary" type="submit" style={{ marginTop: "20px" }}>
            S'inscrire
          </Button>
        </Form>
        <Row className="py-3">
          <Col>
            Déjà inscrit ? <Link to="/login">Connexion</Link>
          </Col>
        </Row>
      </div>
    </MainScreen>
  );
};

export default RegisterScreen;
