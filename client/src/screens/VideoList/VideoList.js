import React, { useEffect, useState } from "react";
import { Button, Card } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { listVideos } from "../../actions/videoActions";
import MainScreen from "../../components/MainScreen";
import VideoThumbnail from "react-video-thumbnail";
import ReactPlayer from "react-player";
import axios from "axios";
const VideoList = () => {
  const videoList = useSelector((state) => state.videoList);
  const { loading, error } = videoList;
  const [videos, setVideos] = useState([]);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const getAllVideos = async () => {
    const config = {
      headers: {
        Authorization: "application/json",
      },
    };
    const { data } = await axios.get(`/api/videos/allvideos`, config);
    setVideos(data);
  };
  useEffect(() => {
    getAllVideos();
  }, []);

  return (
    <MainScreen title={`Liste des vidéos`}>
      <div
        className="homeVideoContrainer"
        style={{
          display: "flex",
          flexDirection: "row",
          flexWrap: "wrap",
          margin: "20px",
          justifyContent: "center",
        }}
      >
        {videos?.map((video) => (
          <Card
            style={{ width: "18rem", margin: "0 10px", marginBottom: "30px" }}
            key={video._id}
          >
            <div className="player-wrapper">
              <Link to={`/watch/${video._id}/${video.user}`}>
                <ReactPlayer
                  url={video.videoPath}
                  className="react-player"
                  playing
                  width="100%"
                  height="100%"
                  controls={false}
                  playing={false}
                />
              </Link>
            </div>

            <Card.Body>
              <Card.Title>{video.title}</Card.Title>
              <Card.Text>{video.description}</Card.Text>
            </Card.Body>
          </Card>
        ))}
      </div>
    </MainScreen>
  );
};

export default VideoList;
