import React, { useEffect, useState } from "react";
import { Accordion, Badge, Button, Card } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import MainScreen from "../../components/MainScreen";
// import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { listVideos } from "../../actions/videoActions";
import Loading from "../../components/Loading";
import ErrorMessage from "../../components/ErrorMessage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPenAlt, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { deleteVideoAction } from "../../actions/videoActions";
import Swal from "sweetalert2";
const MyVideos = () => {
  const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const videoList = useSelector((state) => state.videoList);
  const { loading, videos, error } = videoList;
  // const [videos, setVideos] = useState([]);

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const videoDelete = useSelector((state) => state.videoDelete);
  const {
    loading: loadingDelete,
    error: errorDelete,
    success: successDelete,
  } = videoDelete;

  const videoCreate = useSelector((state) => state.videoCreate);
  const { success: successCreate } = videoCreate;

  const videoUpdate = useSelector((state) => state.videoUpdate);
  const { success: successUpdate } = videoUpdate;

  const deleteHandler = (id) => {
    Swal.fire({
      title: "Êtes-vous sûr de supprimer cette vidéo?",
      text: "C'est irréversible!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#FF7851",
      cancelButtonColor: "#78c2ad",
      confirmButtonText: "Oui",
      cancelButtonText: "Non",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("Supprimé!", "Votre fichier a été supprimé.", "success");
        dispatch(deleteVideoAction(id));
        Toast.fire({
          icon: "success",
          title: "Vidéo supprimé",
        });
      }
    });
  };

  useEffect(() => {
    dispatch(listVideos());
    if (!userInfo) {
      navigate("/");
    }
    if (successCreate) {
      Toast.fire({
        icon: "success",
        title: "Vidéo ajouté",
      });
    }
  }, [dispatch, userInfo, successDelete, successCreate, successUpdate]);

  return (
    <MainScreen title={`Te revoilà ${userInfo.firstName} !`}>
      <Link to="/createvideo">
        <Button style={{ marginLeft: 10, marginBottom: 6 }} size="lg">
          <Link to="/createvideo">Ajouter une vidéo</Link>
        </Button>
      </Link>
      {error && <ErrorMessage variant="danger">{error}</ErrorMessage>}
      {loading && <Loading />}
      {videos?.map((video) => (
        <Accordion style={{ marginTop: 20, marginBottom: 20 }} key={video._id}>
          <Accordion.Item eventKey="0">
            <Card style={{ margin: 10 }}>
              <Accordion.Header as={Card.Text} variant="link" eventKey="0">
                <Card.Header style={{ display: "flex", width: "100%" }}>
                  <span
                    style={{
                      color: "grey",
                      textDecoration: "none",
                      flex: 1,
                      cursor: "pointer",
                      alignSelf: "center",
                      fontSize: 18,
                    }}
                  >
                    {video.title}
                  </span>
                  <div>
                    <Button href={`/video/${video._id}`}>
                      <FontAwesomeIcon icon={faPenAlt} />
                    </Button>
                    <Button
                      variant="danger"
                      className="mx-2"
                      onClick={() => deleteHandler(video._id)}
                    >
                      <FontAwesomeIcon icon={faTrashAlt} />
                    </Button>
                  </div>
                </Card.Header>
              </Accordion.Header>
              <Accordion.Body eventKey="0">
                <Card.Body>
                  <h4>
                    <Badge variant="success">
                      <a
                        href={video.videoPath}
                        target="_blank"
                        rel="noreferrer"
                      >
                        Prévisualiser{" "}
                      </a>
                    </Badge>
                  </h4>
                  <blockquote className="blockquote mb-0">
                    <p>{video.description}</p>
                    <footer className="blockquote-footer">
                      Ajoutée le {video.createdAt.substring(0, 10)}
                    </footer>
                  </blockquote>
                </Card.Body>
              </Accordion.Body>
            </Card>
          </Accordion.Item>
        </Accordion>
      ))}
    </MainScreen>
  );
};

export default MyVideos;
