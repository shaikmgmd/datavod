import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import ErrorMessage from "../components/ErrorMessage";
import Loading from "../components/Loading";
import MainScreen from "../components/MainScreen";
import { Button, Card, Form } from "react-bootstrap";
import axios from "axios";
import { deleteVideoAction, updateVideoAction } from "../actions/videoActions";

function SingleVideo() {
  const [title, setTitle] = useState();
  const [description, setDescription] = useState();
  const [videoPath, setVideoPath] = useState();
  const [date, setDate] = useState("");
  const { id } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const videoUpdate = useSelector((state) => state.videoUpdate);
  const { loading, error } = videoUpdate;

  const videoDelete = useSelector((state) => state.videoDelete);
  const { loading: loadingDelete, error: errorDelete } = videoDelete;

  const deleteHandler = (id) => {
    if (window.confirm("Êtes-vous sûr?")) {
      dispatch(deleteVideoAction(id));
    }
    navigate("/myvideos");
  };

  useEffect(() => {
    const fetching = async () => {
      const { data } = await axios.get(`/api/videos/${id}`);
      setTitle(data.title);
      setDescription(data.description);
      setVideoPath(data.videoPath);
      setDate(data.updatedAt);
    };
    fetching();
  }, [id, date]);

  const resetHandler = () => {
    setTitle("");
    setVideoPath("");
    setDescription("");
  };

  const updateHandler = (e) => {
    e.preventDefault();
    dispatch(updateVideoAction(id, title, description, videoPath));
    if (!title || !description || !videoPath) return;
    resetHandler();
    navigate("/myvideos");
  };

  return (
    <MainScreen title="Modifier la vidéo">
      <Card>
        <Card.Header>Modifications</Card.Header>
        <Card.Body>
          <Form onSubmit={updateHandler}>
            {loadingDelete && <Loading />}
            {error && <ErrorMessage variant="danger">{error}</ErrorMessage>}
            {errorDelete && (
              <ErrorMessage variant="danger">{errorDelete}</ErrorMessage>
            )}
            <Form.Group controlId="title">
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="title"
                placeholder="Enter the title"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </Form.Group>

            <Form.Group
              controlId="description"
              style={{ marginTop: "15px", marginBottom: "20px" }}
            >
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                placeholder="Enter the content"
                rows={4}
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>

            {loading && <Loading size={50} />}
            <Button variant="primary" type="submit">
              Modifier
            </Button>
            <Button
              className="mx-2"
              variant="danger"
              onClick={() => deleteHandler(id)}
            >
              Supprimer
            </Button>
          </Form>
        </Card.Body>

        <Card.Footer className="text-muted">
          Modifiée le - {date.substring(0, 10)}
        </Card.Footer>
      </Card>
    </MainScreen>
  );
}

export default SingleVideo;
