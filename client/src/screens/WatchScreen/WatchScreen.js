import axios from "axios";
import React, { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import ReactPlayer from "react-player";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import MainScreen from "../../components/MainScreen";

const WatchScreen = () => {
  const videoList = useSelector((state) => state.videoList);
  const { loading, error } = videoList;
  const ids = useParams();
  const [video, setVideo] = useState([]);
  const [user, setUser] = useState([]);
  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;
  const getInfosOfVideo = async () => {
    const config = {
      headers: {
        Authorization: "application/json",
      },
    };
    const { data } = await axios.get(`/api/videos/watch/${ids.id}`, config);
    setVideo(data);
  };

  const getUserOfVideo = async () => {
    const config = {
      headers: {
        Authorization: "application/json",
      },
    };
    const { data } = await axios.get(
      `/api/videos/search/${ids.id}/${ids.idUser}`,
      config
    );
    setUser(data[0]);
  };

  useEffect(() => {
    getInfosOfVideo();
    getUserOfVideo();
  }, []);

  return (
    <div>
      {video?.map((v) => (
        <MainScreen title={`${v.title}`}>
          <Card style={{ marginBottom: "30px" }}>
            <Card.Body>{`Vidéo de ${
              user.userName ? user.userName : "..."
            }`}</Card.Body>
          </Card>
          <div style={{ display: "flex", justifyContent: "center" }}>
            <Card
              className="cardStyle"
              style={{
                width: "45%",
                margin: "0 10px",
                marginBottom: "30px",
              }}
              key={v._id}
            >
              <ReactPlayer
                url={v.videoPath}
                className="react-player"
                playing
                width="100%"
                height="100%"
                controls={true}
                playing={true}
              />
            </Card>
          </div>
          <Card style={{ marginBottom: "20px" }}>
            <Card.Body>{`Description : ${
              v.description ? v.description : "..."
            }`}</Card.Body>
          </Card>
          {/* <p>{`Vidéo de `}</p> */}
        </MainScreen>
      ))}
    </div>
  );
};

export default WatchScreen;
