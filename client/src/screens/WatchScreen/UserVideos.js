import React, { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import MainScreen from "../../components/MainScreen";
import ReactPlayer from "react-player";
import axios from "axios";
import ErrorMessage from "../../components/ErrorMessage";
import Loading from "../../components/Loading";
const UserVideos = () => {
  const [videos, setVideos] = useState([]);
  const [user, setUser] = useState([]);
  const [message, setMessage] = useState(null);
  const [loading, setLoading] = useState(false);

  const id = useParams();
  const getVideosOfUser = async () => {
    const config = {
      headers: {
        Authorization: "application/json",
      },
    };
    const { data } = await axios.get(
      `/api/videos/uservideos/${id.idUser}`,
      config
    );

    if (data.length == 0) {
      setMessage("Cet utilisateur n'a pas de vidéo");
    } else {
      setMessage(null);
    }
    setVideos(data);
  };

  const getUser = async () => {
    setLoading(true);
    const config = {
      headers: {
        Authorization: "application/json",
      },
    };
    const { data } = await axios.get(`/api/users/infos/${id.idUser}`, config);

    setUser(data[0]);
    setLoading(false);
  };

  useEffect(() => {
    getVideosOfUser();
    getUser();
  }, []);

  return (
    <MainScreen title={`Vidéos de ${user.userName ? user.userName : "..."}`}>
      <div
        className="homeVideoContrainer"
        style={{
          display: "flex",
          flexDirection: "row",
          flexWrap: "wrap",
          margin: "20px",
          justifyContent: "center",
        }}
      >
        {loading && <Loading />}
        {message && <ErrorMessage variant="dark">{message}</ErrorMessage>}
        {videos?.map((video) => (
          <Card
            style={{ width: "18rem", margin: "0 10px", marginBottom: "30px" }}
            key={video._id}
          >
            <div className="player-wrapper">
              <Link to={`/watch/${video._id}/${video.user}`}>
                <ReactPlayer
                  url={video.videoPath}
                  className="react-player"
                  playing
                  width="100%"
                  height="100%"
                  controls={false}
                  playing={false}
                />
              </Link>
            </div>

            <Card.Body>
              <Card.Title>{video.title}</Card.Title>
              <Card.Text>{video.description}</Card.Text>
            </Card.Body>
          </Card>
        ))}
      </div>
    </MainScreen>
  );
};

export default UserVideos;
