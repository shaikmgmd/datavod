import React, { useEffect, useState } from "react";
import { Button, Card, ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import MainScreen from "../../components/MainScreen";

import axios from "axios";
const UsersScreen = () => {
  const [users, setUsers] = useState([]);

  const getAllUsers = async () => {
    const config = {
      headers: {
        Authorization: "application/json",
      },
    };
    const { data } = await axios.get(`/api/users/allusers`, config);
    setUsers(data);
  };
  useEffect(() => {
    getAllUsers();
  }, []);

  return (
    <MainScreen title={`Liste des utilisateurs`}>
      <div
        className="homeVideoContrainer"
        style={{
          display: "flex",
          flexDirection: "row",
          flexWrap: "wrap",
          margin: "20px",
        }}
      >
        {users?.map((user) => (
          <Card
            style={{ width: "18rem", margin: "0 10px", marginBottom: "30px" }}
            key={user._id}
          >
            <Card.Body>
              <Card.Title style={{ fontSize: "30" }}>
                {user.userName}
              </Card.Title>
            </Card.Body>
            <Button variant="primary">
              <Link to={`/uservideos/${user._id}`}>Voir</Link>
            </Button>
          </Card>
        ))}
      </div>
    </MainScreen>
  );
};

export default UsersScreen;
